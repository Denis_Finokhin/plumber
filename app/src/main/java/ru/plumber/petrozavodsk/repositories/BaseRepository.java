package ru.plumber.petrozavodsk.repositories;

import java.util.List;

/**
 * Created by Den on 22/05/2017.
 */

public class BaseRepository<T> implements IRepository<T>
{

    @Override
    public void add(T item) {

    }

    @Override
    public void update(T item) {

    }

    @Override
    public void remove(T item) {

    }

    @Override
    public T get(int i) {
        return null;
    }

    @Override
    public List<T> getAll() {
        return null;
    }
}
