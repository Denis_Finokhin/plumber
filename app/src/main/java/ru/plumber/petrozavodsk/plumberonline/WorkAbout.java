package ru.plumber.petrozavodsk.plumberonline;

/**
 * Created by Den on 21/05/2017.
 */

public class WorkAbout
{
    private String Name;
    private int Image;
    private String Descr;

    public WorkAbout(String Name, int Image, String  Descr){
        this.Name=Name;
        this.Descr=Descr;
        this.Image=Image;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int Image) {
        this.Image = Image;
    }

    public  String getDescr(){
        return Descr;
    }

    public void setDescr(String Descr){
        this.Descr=Descr;
    }
}
