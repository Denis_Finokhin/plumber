package ru.plumber.petrozavodsk.plumberonline;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Switch;
import android.widget.TextView;

public class MySetting extends AppCompatActivity {
    public static final String PREFS_NAME = "MyPrefsFile";
    private SharedPreferences settings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_setting);

        setupActionBar();

        settings = getSharedPreferences(PREFS_NAME, 0);
        ((TextView)findViewById(R.id.fio_name)).setText(settings.getString("fio_name", null));
        ((TextView)findViewById(R.id.fio_surname)).setText(settings.getString("fio_surname", null));
        ((TextView)findViewById(R.id.fio_email)).setText(settings.getString("fio_email", null));
        ((TextView)findViewById(R.id.zakaz_adress)).setText(settings.getString("zakaz_adress", null));
        ((TextView)findViewById(R.id.zakaz_phone)).setText(settings.getString("zakaz_phone", null));
        ((Switch) findViewById(R.id.switch_garanty)).setChecked(settings.getBoolean("switch_garanty",false));
        ((Switch) findViewById(R.id.switch_dop)).setChecked(settings.getBoolean("switch_dop",false));

    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {

            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(R.layout.abs_layout);
            ((TextView)findViewById(R.id.myTitle)).setText(R.string.menu_setting);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            //onBackPressed();



            SharedPreferences.Editor editor = settings.edit();
            editor.putString("fio_name", ((TextView)findViewById(R.id.fio_name)).getText().toString());
            editor.putString("fio_surname", ((TextView)findViewById(R.id.fio_surname)).getText().toString());
            editor.putString("fio_email", ((TextView)findViewById(R.id.fio_email)).getText().toString());
            editor.putString("zakaz_adress", ((TextView)findViewById(R.id.zakaz_adress)).getText().toString());
            editor.putString("zakaz_phone", ((TextView)findViewById(R.id.zakaz_phone)).getText().toString());
            editor.putBoolean("switch_garanty",((Switch) findViewById(R.id.switch_garanty)).isChecked());
            editor.putBoolean("switch_dop",((Switch) findViewById(R.id.switch_dop)).isChecked());
            editor.commit();


            this.finish();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }



}
