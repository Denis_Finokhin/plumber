package ru.plumber.petrozavodsk.plumberonline;

/**
 * Created by Den on 21/05/2017.
 */

public class Product
{
    private int Id;
    private int Image;
    private String Name;
    private String Price;

    public  Product(){}
    public  Product(int id,  int Image,String Name,String  Price)
    {
        this.Id=id;
        this.Image=Image;
        this.Name=Name;
        this.Price=Price;
    }

    public  Product(  int Image,String Name,String  Price)
    {
        this.Image=Image;
        this.Name=Name;
        this.Price=Price;
    }

    public  int getId(){
        return  Id;}

    public  void  setId(int Id){
        this.Id=Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int Image) {
        this.Image = Image;
    }

    public  String getPrice(){
        return Price;
    }

    public void setPrice(String Price){
        this.Price=Price;
    }
}
