package ru.plumber.petrozavodsk.plumberonline;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import ru.plumber.petrozavodsk.DB.DatabaseHandler;

public class SendActivity extends AppCompatActivity {

   private int mYear;
    private int mMonth;
    private int mDay;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        Intent intent = getIntent();


        int pId  = intent.getIntExtra("PRODUCT_ID",-1);
            if (pId <0)
        {
            this.finish();
            return;
        }

        setContentView(R.layout.activity_send);
        DatabaseHandler db = new DatabaseHandler(this);
        Product product= db.getProduct(pId);

        ImageView iview=(ImageView) findViewById(R.id.sendImg);
        iview.setImageResource(product.getImage());
        TextView txtTitle=(TextView)findViewById(R.id.sendTitle);
        txtTitle.setText(product.getName());

        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);

        EditText editTextFromDate = (EditText) findViewById(R.id.sendInputDate);
        editTextFromDate.setText
                (
                new StringBuilder().append(String.format("%02d",day)).append(".").append(String.format("%02d",month+1)).append(".").append(year)
                );
        editTextFromDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //To show current date in the datepicker
                Calendar mcurrentDate=Calendar.getInstance();
                 mYear=mcurrentDate.get(Calendar.YEAR);
                 mMonth=mcurrentDate.get(Calendar.MONTH);
                 mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(SendActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        EditText editTextFromDate = (EditText) findViewById(R.id.sendInputDate);
                        String myFormat = "dd/mm/yyyy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);

                        editTextFromDate.setText(new StringBuilder().append(String.format("%02d",selectedday)).append(".").append(String.format("%02d",selectedmonth+1)).append(".").append(selectedyear));
                    }
                },mYear, mMonth, mDay);
                mDatePicker.setTitle("Выберите день");
                mDatePicker.show();
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            //this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
       this.finish();
    }
}
