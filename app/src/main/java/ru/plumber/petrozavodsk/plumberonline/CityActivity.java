package ru.plumber.petrozavodsk.plumberonline;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import ru.plumber.petrozavodsk.DB.DatabaseHandler;

/**
 * Created by Den on 22/05/2017.
 */

public class CityActivity extends AppCompatActivity {

    public static final String PREFS_NAME = "MyPrefsFile";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String selectedCity = settings.getString("homeCity", null);
        if (selectedCity==null)
        {
            DatabaseHandler db = new DatabaseHandler(this);
           db.addProduct(new Product(R.drawable.p1, "Замена отопления радиаторов", "от 1990 р."));
                db.addProduct(new Product(R.drawable.p2, "Замена унитаза", "1990 р."));
                db.addProduct(new Product(R.drawable.p3, "Замена и установка бойлера", "2990 р."));
                db.addProduct(new Product(R.drawable.p4, "Устранение течей, замена смесителя", "990 р."));
                db.addProduct(new Product(R.drawable.washmachine, "Замена и установка бойлера", "1490 р."));
                db.addProduct(new Product(R.drawable.piping, "Замена труб", "550 р./метр"));


            setTheme(R.style.AppTheme_SelectCity);
            setContentView(R.layout.select_city_first);
            String[]  cities=  getResources().getStringArray(R.array.city_list);
            ListView listView = (ListView)findViewById(R.id.cityView);
            CustomListAdapter adapter = new CustomListAdapter(this , R.layout.my_list_item,
                    new ArrayList<String>(Arrays.asList(cities)));

            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id)
                {

                    String item = ((TextView)view).getText().toString();

                    SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("homeCity", item);

                    // Commit the edits!
                    editor.commit();
                    LoadMainActivity();
                }
            });
        }else
        {
            LoadMainActivity();
        }
    }

    protected void  LoadMainActivity()
    {
        Intent intent=new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
        this.finish();

    }

}