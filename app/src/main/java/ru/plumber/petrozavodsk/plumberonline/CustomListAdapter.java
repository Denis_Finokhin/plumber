package ru.plumber.petrozavodsk.plumberonline;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created by Den on 22/05/2017.
 */


public class CustomListAdapter extends ArrayAdapter<String> {

    private Context mContext;
    private int id;
    private List<String> items ;

    public CustomListAdapter(Context context, int textViewResourceId , List<String> list )
    {
        super(context, textViewResourceId, list);
        mContext = context;
        id = textViewResourceId;
        items = list ;
    }


}