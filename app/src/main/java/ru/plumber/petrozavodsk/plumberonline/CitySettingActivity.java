package ru.plumber.petrozavodsk.plumberonline;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class CitySettingActivity extends AppCompatActivity {

    public static final String PREFS_NAME = "MyPrefsFile";
    private  ListView lvCity;
    private int pos=1;
    private Activity mcontext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);
        mcontext=this;
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String selectedCity = settings.getString("homeCity", null);

        String[] cities  = getResources().getStringArray(R.array.city_list);
         lvCity = (ListView) findViewById(R.id.CitySelector);
        ArrayAdapter<String> adapterCity= new ArrayAdapter<String>(this, R.layout.cityselect_list_item,R.id.lblCity,  cities);
        lvCity.setAdapter(adapterCity);


        if (!selectedCity.equals(null))
        {

             pos= adapterCity.getPosition(selectedCity);

            lvCity.clearFocus();
            lvCity.requestFocusFromTouch();
            lvCity.post(new Runnable() {
                @Override
                public void run() {
                    lvCity.setItemChecked(pos, true);
                    lvCity.setSelection(pos);

                }
            });


        }

        lvCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position,
                                    long id)
            {
                ListView lvCity = (ListView) findViewById(R.id.CitySelector);
                String  city = (String)lvCity.getItemAtPosition(position);
                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("homeCity", city);
                // Commit the edits!
                editor.commit();
                Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
                mcontext.finish();

            }
        });


        setupActionBar();
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(R.layout.abs_layout);
            ((TextView)findViewById(R.id.myTitle)).setText(R.string.title_activity_city);
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            //onBackPressed();
            Intent intent=new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





}
