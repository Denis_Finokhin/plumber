package ru.plumber.petrozavodsk.repositories;

import java.util.List;

/**
 * Created by Den on 22/05/2017.
 */
public interface IRepository<T> {
    void add(T item);
    void update(T item);
    void remove(T item);
    T get(int i);
    List<T> getAll();
}