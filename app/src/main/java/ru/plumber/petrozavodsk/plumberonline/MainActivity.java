package ru.plumber.petrozavodsk.plumberonline;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.plumber.petrozavodsk.DB.DatabaseHandler;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private List<Product> products=new ArrayList<Product>();
    private ArrayList<WorkAbout> workabouts=new ArrayList<WorkAbout>();

    public static final String PREFS_NAME = "MyPrefsFile";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        DatabaseHandler db = new DatabaseHandler(this);
        products = db.getAllProducts();


        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String selectedCity = settings.getString("homeCity", null);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);


        toolbar.setTitle(R.string.app_name);

        //((TextView)toolbar.findViewById(R.id.toolbar_title)).setText(R.string.app_name);
       // ((TextView)toolbar.findViewById(R.id.toolbar_title2)).setText(selectedCity);
        //toolbar.setLogo(R.drawable.menu_logo);
        toolbar.setSubtitle(selectedCity);
        //((ImageView)toolbar.findViewById(R.id.toolbar_title_img)).setText(selectedCity);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        workabouts.add(new WorkAbout("Выносим мусор",R.drawable.broom,"Наши сантехники выноят весь габаритный мусор до мусорных баков"));
        workabouts.add(new WorkAbout("Работаем в бахилах",R.drawable.shoes,"Либо просто снимаем обувь. \n Все по человечески."));
        workabouts.add(new WorkAbout("Гарантия 1 год",R.drawable.prize,"на все виды работ по договору, можете быть уверены в качестве наших услуг."));
        workabouts.add(new WorkAbout("Работаем без выходных",R.drawable.work,"С 9.00 до 20.00. Даже 1 января."));

        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new MyAdapter(this));
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // start-media code could go in fragment or adapter



                GridView gridview = (GridView) findViewById(R.id.gridview);
                Product item=(Product)gridview.getAdapter().getItem(position);
                Toast.makeText(getApplicationContext(),item.getId()+": "+ item.getName(),Toast.LENGTH_SHORT).show();
                Animation animation1 = new AlphaAnimation(0.3f, 1.0f);
                animation1.setDuration(4000);
                view.startAnimation(animation1);
                Intent intent = new Intent(getApplicationContext(), SendActivity.class);
                intent.putExtra("PRODUCT_ID", item.getId());
                startActivity(intent);





            }
        });
        gridview.setNumColumns(2);
        gridview.setVerticalScrollBarEnabled(false);
        ListView listabout=(ListView) findViewById(R.id.listAbout);
        listabout.setAdapter(new MyAdapterInfo(this));
    }

    /////////////
    public class MyAdapter extends BaseAdapter {

        private Context mContext;

        public MyAdapter(Context c) {
            mContext = c;
        }

        @Override
        public int getCount() {
            return products.size();
        }

        @Override
        public Object getItem(int arg0) {
            return products.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View grid;

            if(convertView==null){
                grid = new View(mContext);
                LayoutInflater inflater=getLayoutInflater();
                grid=inflater.inflate(R.layout.mygrid_layout, parent, false);
            }else{
                grid = (View)convertView;
            }

            ImageView imageView = (ImageView)grid.findViewById(R.id.image);
            imageView.setImageResource(products.get(position).getImage());
            TextView textName=(TextView)grid.findViewById(R.id.productName);
            textName.setText(products.get(position).getName());
            TextView textPrice=(TextView)grid.findViewById(R.id.productPrice);
            textPrice.setText(products.get(position).getPrice());

            return grid;
        }
    }
    ////////////

    public class MyAdapterInfo extends BaseAdapter {

        private Context mContext;

        public MyAdapterInfo(Context c) {
            mContext = c;
        }

        @Override
        public int getCount() {
            return workabouts.size();
        }

        @Override
        public Object getItem(int arg0) {
            return workabouts.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View  getView(int position, View convertView, ViewGroup parent) {

            View grid;

            if(convertView==null){
                grid = new View(mContext);
                LayoutInflater inflater=getLayoutInflater();
                grid=inflater.inflate(R.layout.mymain_about_list, parent, false);
            }else{
                grid = (View)convertView;
            }
            ImageView imageView = (ImageView)grid.findViewById(R.id.info_img);
            imageView.setImageResource(workabouts.get(position).getImage());
            TextView textName=(TextView)grid.findViewById(R.id.info_img_title);
            textName.setText(workabouts.get(position).getName());
            TextView textdescr=(TextView)grid.findViewById(R.id.info_img_descr);
            textdescr.setText(workabouts.get(position).getDescr());


            return grid;
        }
    }

    //////////////////

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), MySetting.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_city) {
            Intent intent = new Intent(getApplicationContext(), CitySettingActivity.class);
            startActivity(intent);
            this.finish();

        } else if (id == R.id.nav_zakaz) {

        } else if (id == R.id.nav_setting) {
            Intent intent = new Intent(getApplicationContext(), MySetting.class);
            startActivity(intent);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

