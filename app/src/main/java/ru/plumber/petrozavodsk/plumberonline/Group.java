package ru.plumber.petrozavodsk.plumberonline;

import java.util.ArrayList;

/**
 * Created by Den on 21/05/2017.
 */

public class Group
{
    private String Name;
    private ArrayList<Product> Items;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public ArrayList<Product> getItems() {
        return Items;
    }

    public void setItems(ArrayList<Product> Items) {
        this.Items = Items;
    }
}
